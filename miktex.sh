#
# MiKTeX
#
# see: https://miktex.org/howto/install-miktex-unx
#
# ensure super user
if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi


echo "Installing MiKTeX"
# register GPG key
apt-key adv --no-tty --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys D6BC243565B2087BC3F897C9277A7293F59E4889
# register install source
echo "deb http://miktex.org/download/ubuntu bionic universe" | sudo tee /etc/apt/sources.list.d/miktex.list
# install
apt-get update && apt-get install miktex -y
# run MiKTeX setup
miktexsetup --shared=yes finish
