#
# Euroscope Latest Beta
#
# ensure super user
if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

echo "Installing Euroscope"
# dependencies
winetricks vcrun2010
# download
wget -P ~/Downloads/ http://www.euroscope.hu/install/EuroScopeBeta32a23.zip
wget -P ~/Downloads/ https://www.euroscope.hu/wp/wp-content/uploads/2019/05/AsrcPlus2.ico
# install
mkdir /opt/euroscope && unzip ~/Downloads/EuroScopeBeta* -d /opt/euroscope
mv ~/Downloads/AsrcPlus2.ico /opt/euroscope/icon.ico
rm ~/Downloads/EuroScopeBeta* ~/Downloads/AsrcPlus2.ico -f
# symlink to system resources
ln -sf /opt/euroscope /usr/bin/euroscope
# create shortcut
cat <<EOF >/usr/share/applications/euroscope.desktop
[Desktop Entry]
Version = 1.0
Type = Application
Terminal = false
Name = EuroScope
Exec = /usr/bin/wine /usr/bin/euroscope/EuroScope.exe
Icon = /opt/euroscope/icon.ico
Categories = Application;
EOF
