#
# TeamViewer 11 Quick Support
#

echo "Installing TeamViewer 11 Quick Support"
# install
#mkdir ~/.teamviewer11_qs && wget --output-document - https://download.teamviewer.com/download/version_11x/teamviewer_qs.tar.gz | tar --extract --gzip --strip 1 --directory ~/.teamviewer11_qs --file -
# create shortcut
wget https://www.teamviewer.com/wp-content/themes/tv-wordpress-theme/dist/media/favicon.png --output-document ~/.teamviewer11_qs/icon.png
cat <<EOF >~/.local/share/applications/teamviewer11_qs.desktop
[Desktop Entry]
Version = 1.0
Type = Application
Terminal = false
Name = TeamViewer 11 QuickSupport
Exec = env LC_ALL=C /home/prodrigues/.teamviewer11_qs/teamviewer
Icon = /home/prodrigues//.teamviewer11_qs/icon.png
Categories = Application;
EOF
