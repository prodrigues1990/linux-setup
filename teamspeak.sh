#
# TeamSpeak
#
# ensure super user
if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

# download
#wget -P ~/Downloads/ https://files.teamspeak-services.com/releases/client/3.3.2/TeamSpeak3-Client-linux_amd64-3.3.2.run
# install
chmod +x ~/Downloads/TeamSpeak3-Client-linux_amd64-3.3.2.run
mkdir /opt/teamspeak-client
~/Downloads/TeamSpeak3-Client-linux_amd64-3.3.2.run --target /opt/teamspeak-client
rm ~/Downloads/TeamSpeak3-Client-linux_amd64-3.3.2.run -f
# symlink to system resources
ln -sf /opt/teamspeak-client/ts3client_runscript.sh /usr/bin/ts3client_runscript.sh
# create shortcut
cat <<EOF >/usr/share/applications/teamspeak.desktop
[Desktop Entry]
Version = 3.3
Type = Application
Terminal = false
Name = TeamSpeak
Exec = /usr/bin/ts3client_runscript.sh
Icon = /opt/teamspeak-client/styles/default/logo-128x128.png
Categories = Application;
EOF
