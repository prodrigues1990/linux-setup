#
# Popcorn Time
#
# ensure super user
if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

echo "Installing Popcorn Time"
# dependencies
apt-get install libcanberra-gtk-module libgconf-2-4 -y
# download
wget -P ~/Downloads/ https://get.popcorntime.sh/build/Popcorn-Time-0.3.10-Linux-64.tar.xz
# install
mkdir /opt/popcorntime && tar Jxf ~/Downloads/Popcorn-Time-* -C /opt/popcorntime
rm ~/Downloads/Popcorn-Time-* -f
# symlink to system resources
ln -sf /opt/popcorntime/Popcorn-Time /usr/bin/Popcorn-Time
# create shortcut
cat <<EOF >/usr/share/applications/popcorntime.desktop
[Desktop Entry]
Version = 1.0
Type = Application
Terminal = false
Name = Popcorn Time
Exec = /usr/bin/Popcorn-Time
Icon = /opt/popcorntime/src/app/images/icon.png
Categories = Application;
EOF
