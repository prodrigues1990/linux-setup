#
# NodeJS, NPM, Bower, and Polymer CLI
#
# see: https://linuxize.com/post/how-to-install-node-js-on-ubuntu-18.04/
#      https://polymer-library.polymer-project.org/2.0/docs/tools/polymer-cli
#      https://github.com/Polymer/tools/issues/2498
#
# ensure super user
if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

echo "Installing NodeJS 10 and NPM"
# add node GPG key
curl -sL https://deb.nodesource.com/setup_10.x | bash -
# install node and npm
apt-get install -y nodejs
# update npm
npm install npm@latest -g

echo "Installing Bower"
npm install -g bower

echo "Installing Polymer-CLI"
npm install -g polymer-cli --unsafe-perm
